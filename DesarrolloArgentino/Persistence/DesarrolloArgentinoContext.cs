﻿using DesarrolloArgentino.Core.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DesarrolloArgentino.Persistence
{
    public class DesarrolloArgentinoContext : IdentityDbContext<IdentityUser>
    {
        public DesarrolloArgentinoContext()
            : base("name=DARContext", throwIfV1Schema: false)
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public static DesarrolloArgentinoContext Create()
        {
            return new DesarrolloArgentinoContext();
        }
        //public virtual DbSet<Tag> Tags { get; set; }
        public virtual DbSet<Noticia> Noticias { get; set; }
        public virtual DbSet<Actividad> Actividades { get; set; }
        public virtual DbSet<Prensa> Prensas { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }

        public virtual DbSet<Categoria> Categorias { get; set; }
        public virtual DbSet<Image> Images { get; set; }

        public virtual DbSet<CategoriaDespido> CategoriasDespidos { get; set; }
        public virtual DbSet<DatosEmpresa> DatosEmpresa { get; set; }
        public virtual DbSet<DatosPersonales> DatosPersonales { get; set; }
        public virtual DbSet<Opciones> Opciones { get; set; }
        public virtual DbSet<InfoDespido> InfoDespidos { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            //modelBuilder.Configurations.Add(new CourseConfiguration());
            modelBuilder.Entity<Actividad>().ToTable("Actividades");
            modelBuilder.Entity<Prensa>().ToTable("Prensas");

        }

        public class ApplicationUser : IdentityUser
        {
            public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
            {
                // Tenga en cuenta que el valor de authenticationType debe coincidir con el definido en CookieAuthenticationOptions.AuthenticationType
                var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ExternalCookie);

                //userIdentity.AddClaim(new Claim(ClaimTypes.Name, context.UserName));
                userIdentity.AddClaim(new Claim(ClaimTypes.Role, "user"));
                //userIdentity.AddClaim(new Claim("sub", context.UserName));
                // Agregar aquí notificaciones personalizadas de usuario
                return userIdentity;
            }
        }


    }
}