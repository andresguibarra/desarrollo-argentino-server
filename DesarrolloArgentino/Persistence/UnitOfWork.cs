﻿using DesarrolloArgentino.Core;
using DesarrolloArgentino.Core.Repositories;
using DesarrolloArgentino.Persistence.Repositories;

namespace DesarrolloArgentino.Persistence
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DesarrolloArgentinoContext _context;

        public UnitOfWork(DesarrolloArgentinoContext context)
        {
            _context = context;

            Noticias = new NoticiaRepository(_context);
            Categorias = new CategoriasRepository(_context);
            Actividades = new ActividadRepository(_context);
            Prensas = new PrensaRepository(_context);
            CategoriasDespidos = new CategoriaDespidoRepository(_context);
            DatosEmpresas = new DatoEmpresaRepository(_context);
            DatosPersonales = new DatoPersonalRepository(_context);
            Opciones = new OpcionRepository(_context);
            InfoDespidos = new InfoDespidoRepository(_context);

            //Courses = new CourseRepository(_context);
            //Authors = new AuthorRepository(_context);
        }

        //public ICourseRepository Courses { get; private set; }
        public INoticiaRepository Noticias { get; private set; }
        public ICategoriaRepository Categorias { get; private set; }
        public IActividadRepository Actividades { get; }
        public IPrensaRepository Prensas { get; }
        public ICategoriasDespidosRepository CategoriasDespidos { get; }
        public IDatoEmpresaRepository DatosEmpresas { get; }
        public IDatoPersonalRepository DatosPersonales { get; }
        public IOpcionRepository Opciones { get; }
        public IInfoDespidoRepository InfoDespidos { get; }

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }

}