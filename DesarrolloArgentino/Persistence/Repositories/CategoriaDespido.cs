﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class CategoriaDespidoRepository : Repository<CategoriaDespido>, ICategoriasDespidosRepository
    {
        public CategoriaDespidoRepository(DesarrolloArgentinoContext context) : base(context)
        {
        }
    }
}