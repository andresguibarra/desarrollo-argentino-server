﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class DatoPersonalRepository : Repository<DatosPersonales>, IDatoPersonalRepository
    {
        public DatoPersonalRepository(DesarrolloArgentinoContext context) : base(context)
        {

        }
    }
}