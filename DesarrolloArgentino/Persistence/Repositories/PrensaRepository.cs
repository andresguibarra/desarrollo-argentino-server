﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class PrensaRepository : Repository<Prensa>, IPrensaRepository
    {
        public PrensaRepository(DesarrolloArgentinoContext context) : base(context)
        {
        }
    }
}