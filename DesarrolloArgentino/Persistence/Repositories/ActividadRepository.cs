﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class ActividadRepository : Repository<Actividad>, IActividadRepository
    {
        public ActividadRepository(DesarrolloArgentinoContext context) : base(context)
        {
        }
    }
}