﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class CategoriasRepository : Repository<Categoria>, ICategoriaRepository
    {
        public CategoriasRepository(DesarrolloArgentinoContext context) : base(context)
        {
        }



    }
}