﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class DatoEmpresaRepository : Repository<DatosEmpresa>, IDatoEmpresaRepository
    {
        public DatoEmpresaRepository(DesarrolloArgentinoContext context) : base(context)
        {

        }


    }
}