﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class OpcionRepository : Repository<Opciones>, IOpcionRepository
    {
        public OpcionRepository(DesarrolloArgentinoContext context) : base(context)
        {

        }
    }
}