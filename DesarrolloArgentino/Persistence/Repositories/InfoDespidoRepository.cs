using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Core.Repositories;

namespace DesarrolloArgentino.Persistence.Repositories
{
    public class InfoDespidoRepository : Repository<InfoDespido>, IInfoDespidoRepository
    {
        public InfoDespidoRepository(DesarrolloArgentinoContext context) : base(context)
        {

        }
    }
}