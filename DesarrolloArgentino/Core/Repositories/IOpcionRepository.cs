﻿using DesarrolloArgentino.Core.Entities;

namespace DesarrolloArgentino.Core.Repositories
{
    public interface IOpcionRepository : IRepository<Opciones>
    {
    }
}
