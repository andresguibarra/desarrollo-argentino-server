﻿using DesarrolloArgentino.Core.Entities;

namespace DesarrolloArgentino.Core.Repositories
{
    public interface ICategoriasDespidosRepository : IRepository<CategoriaDespido>
    {
    }
}
