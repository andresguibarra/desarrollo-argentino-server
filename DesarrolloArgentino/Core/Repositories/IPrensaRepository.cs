﻿using DesarrolloArgentino.Core.Entities;

namespace DesarrolloArgentino.Core.Repositories
{
    public interface IPrensaRepository : IRepository<Prensa>
    {

    }
}
