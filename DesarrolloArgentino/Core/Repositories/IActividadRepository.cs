﻿using DesarrolloArgentino.Core.Entities;

namespace DesarrolloArgentino.Core.Repositories
{
    public interface IActividadRepository : IRepository<Actividad>
    {

    }
}
