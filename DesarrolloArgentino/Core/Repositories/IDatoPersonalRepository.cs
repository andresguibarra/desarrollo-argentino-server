﻿using DesarrolloArgentino.Core.Entities;

namespace DesarrolloArgentino.Core.Repositories
{
    public interface IDatoPersonalRepository : IRepository<DatosPersonales>
    {
    }
}
