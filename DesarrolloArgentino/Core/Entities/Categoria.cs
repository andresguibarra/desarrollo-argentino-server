﻿using System.ComponentModel.DataAnnotations;

namespace DesarrolloArgentino.Core.Entities
{
    public class Categoria
    {
        public Categoria()
        {

        }
        [Key]
        public long CategoriaId { get; set; }
        public string Nombre { get; set; }
    }
}