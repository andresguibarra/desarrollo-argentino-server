﻿using System;
using System.ComponentModel.DataAnnotations;

namespace DesarrolloArgentino.Core.Entities
{
    public abstract class GenericPost
    {

        public string Titulo { get; set; }

        public string Descripcion { get; set; }
        public string Autor { get; set; }

        public string Contenido { get; set; }

        public string ImageUrl { get; set; }
        [Required]
        public long CategoriaId { get; set; }

        public virtual Categoria Categoria { get; set; }

        public DateTime? FechaCreacion { get; set; }

        public DateTime? FechaUltimaModificacion { get; set; }
    }

    public class GenericPostTSQL : GenericPost
    {
        public long Id { get; set; }
    }
}