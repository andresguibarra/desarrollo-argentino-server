﻿namespace DesarrolloArgentino.Core.Entities
{
    public class Opciones
    {
        public long OpcionesId { get; set; }

        public string Nombre { get; set; }
    }
}