namespace DesarrolloArgentino.Core.Entities
{
    public class InfoDespido
    {
        public long InfoDespidoId { get; set; }
        public long CategoriaDespidoId { get; set; }
        public long DatosEmpresaId { get; set; }
        public long DatosPersonalesId { get; set; }
        public long OpcionesId { get; set; }
        //public virtual CategoriaDespido CategoriaDespido { get; set; }
        public virtual CategoriasDespido CategoriaDespido { get; set; }
        public virtual DatosEmpresa DatoEmpresa { get; set; }
        public virtual DatosPersonales DatoPersonal { get; set; }
        public virtual OpcionesDespido OpcionDespido { get; set; }
        public string Comentarios { get; set; }
        public string FuncionEmpresa { get; set; }

    }

    public enum CategoriasDespido
    {
        MeEcharon = 1,
        TengoMiedoQueMeEchen = 2,
        EstanEchandoGente = 3
    }

    public enum OpcionesDespido
    {
        EstanEchandoGenteDeLaEmpresa = 1,
        EmpresaLeVaMal = 2,
        NoEstanContentosConMiTrabajo = 3,
        Reestructuracion = 4,
        CerroLaEmpresa = 5
    }
}