namespace DesarrolloArgentino.Core.Entities
{
    public enum ApplicationTypes
    {
        JavaScript = 0,
        NativeConfidential = 1
    };
}