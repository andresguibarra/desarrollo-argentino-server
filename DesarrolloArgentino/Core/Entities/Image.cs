﻿namespace DesarrolloArgentino.Core.Entities
{
    public class Image
    {
        public long ImageId { get; set; }
        public long Url { get; set; }
        public long Description { get; set; }
    }
}