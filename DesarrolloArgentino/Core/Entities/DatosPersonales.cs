﻿namespace DesarrolloArgentino.Core.Entities
{
    public class DatosPersonales
    {
        public long DatosPersonalesId { get; set; }
        public string NombreApellido { get; set; }
        public string DireccionE­Mail { get; set; }
        public string Telefono { get; set; }
        public long? Edad { get; set; }
        public string Ocupacion { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Location { get; set; }
    }
}