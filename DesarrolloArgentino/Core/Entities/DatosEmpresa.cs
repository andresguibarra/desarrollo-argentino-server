﻿namespace DesarrolloArgentino.Core.Entities
{
    public class DatosEmpresa
    {
        public long DatosEmpresaId { get; set; }
        public string NombreDeEmpresa { get; set; }
        public long? CantidadDeEmpleados { get; set; }
        public string Rubro { get; set; }
        public string Provincia { get; set; }
        public string Ciudad { get; set; }
        public string Dirección { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }

    }
}