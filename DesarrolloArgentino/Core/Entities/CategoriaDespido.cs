﻿namespace DesarrolloArgentino.Core.Entities
{
    public class CategoriaDespido
    {
        public long CategoriaDespidoId { get; set; }

        public string Nombre { get; set; }
    }
}