﻿using DesarrolloArgentino.Core.Repositories;
using System;

namespace DesarrolloArgentino.Core
{

    public interface IUnitOfWork : IDisposable
    {
        //ICourseRepository Courses { get; }
        INoticiaRepository Noticias { get; }
        ICategoriaRepository Categorias { get; }
        IActividadRepository Actividades { get; }
        IPrensaRepository Prensas { get; }

        ICategoriasDespidosRepository CategoriasDespidos { get; }
        IDatoEmpresaRepository DatosEmpresas { get; }
        IDatoPersonalRepository DatosPersonales { get; }
        IOpcionRepository Opciones { get; }
        IInfoDespidoRepository InfoDespidos { get; }

        int Complete();
    }
}
