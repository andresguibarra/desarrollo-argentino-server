using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Models;
using DesarrolloArgentino.Persistence;
using DesarrolloArgentino.Services;
using System;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    [RoutePrefix("api/infodespidos")]
    public class InfoDespidosController : BaseApiController
    {
        [Route("{id:long}", Name = "GetInfoDespidoById")]
        public IHttpActionResult Get(long id)
        {
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var infoDespido = unitOfWork.InfoDespidos.Get(id);
                if (infoDespido != null)
                {
                    return Ok(this.TheModelFactory.Create(infoDespido));
                }
                return NotFound();
            }

        }

        public async Task<IHttpActionResult> Get()
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                return Ok((await unitOfWork.InfoDespidos.GetAllAsyncTask()).Select(n => this.TheModelFactory.Create(n)));
            }

        }
        //[Authorize]
        //[Route("Create")]
        public IHttpActionResult Create(CreateInfoDespidoBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model invalid");
                return BadRequest(ModelState);
            }
            var infoDespido = new InfoDespido();
            string subject = "Cuidar Trabajo | ";
            if (model.CategoriaDespidoId == 1)
            {
                subject += "Me echaron";

                infoDespido.CategoriaDespido = CategoriasDespido.MeEcharon;


            }
            else if (model.CategoriaDespidoId == 2)
            {
                subject += "Tengo miedo que me echen";
                infoDespido.CategoriaDespido = CategoriasDespido.TengoMiedoQueMeEchen;
            }
            else
            {
                subject += "Est�n echando gente";
                infoDespido.CategoriaDespido = CategoriasDespido.EstanEchandoGente;

            }
            string mensaje = "Razon : ";
            switch (model.IdOpcionElegida)
            {
                case 1:
                    {
                        mensaje += "Porque est�n echando gente de la empresa";
                        infoDespido.OpcionDespido = OpcionesDespido.EstanEchandoGenteDeLaEmpresa;
                        break;
                    }
                case 2:
                    {
                        mensaje += "Porque a la empresa le va mal";
                        infoDespido.OpcionDespido = OpcionesDespido.EmpresaLeVaMal;
                        break;

                    }
                case 3:
                    {
                        mensaje += "Porque no est�n contentos con mi trabajo";
                        infoDespido.OpcionDespido = OpcionesDespido.NoEstanContentosConMiTrabajo;
                        break;

                    }
                case 4:
                    {
                        mensaje += "Porque no estaban contentos con mi trabajo / reestructuraci�n";
                        infoDespido.OpcionDespido = OpcionesDespido.Reestructuracion;
                        break;

                    }
                case 5:
                    {
                        mensaje += "Porque le va mal a la empresa";
                        infoDespido.OpcionDespido = OpcionesDespido.EmpresaLeVaMal;
                        break;

                    }
                case 6:
                    {
                        mensaje += "Porque cerr� la empresa";
                        infoDespido.OpcionDespido = OpcionesDespido.CerroLaEmpresa;
                        break;

                    }
                default:
                    break;
            }
            mensaje += Environment.NewLine;
            if (!string.IsNullOrEmpty(model.Descripcion))
            {

                mensaje += "Descripcion : " + model.Descripcion;
                mensaje += Environment.NewLine;

                infoDespido.Comentarios = model.Descripcion;
            }
            if (!string.IsNullOrEmpty(model.Funcion))
            {
                mensaje += "Funcion : " + model.Funcion;
                infoDespido.Comentarios = model.Funcion;
                mensaje += Environment.NewLine;

            }
            mensaje += Environment.NewLine;


            var datosEmpresaEntity = new DatosEmpresa();
            string datosEmpresa = "";

            datosEmpresa += "Nombre empresa : " + model.DatosEmpresa.Nombre;
            datosEmpresa += Environment.NewLine;
            datosEmpresa += "Rubro : " + model.DatosEmpresa.Rubro;
            datosEmpresa += Environment.NewLine;
            datosEmpresa += "Cantidad de empleados: " + model.DatosEmpresa.CantidadEmpleados;
            datosEmpresa += Environment.NewLine;
            datosEmpresa += "Ciudad : " + model.DatosEmpresa.Ciudad;
            datosEmpresa += Environment.NewLine;
            datosEmpresa += "Provincia : " + model.DatosEmpresa.Provincia;
            datosEmpresa += Environment.NewLine;
            datosEmpresa += "Direccion : " + model.DatosEmpresa.Direccion;
            datosEmpresa += Environment.NewLine;
            datosEmpresa += Environment.NewLine;


            #region Fill entity

            datosEmpresaEntity.CantidadDeEmpleados = model.DatosEmpresa.CantidadEmpleados;
            datosEmpresaEntity.NombreDeEmpresa = model.DatosEmpresa.Nombre;
            datosEmpresaEntity.Rubro = model.DatosEmpresa.Rubro;
            datosEmpresaEntity.Ciudad = model.DatosEmpresa.Ciudad;
            datosEmpresaEntity.Provincia = model.DatosEmpresa.Provincia;
            datosEmpresaEntity.Direcci�n = model.DatosEmpresa.Direccion;

            #endregion

            var location = LocationService.GetLocation(datosEmpresaEntity.Direcci�n, datosEmpresaEntity.Provincia,
                datosEmpresaEntity.Ciudad);


            datosEmpresaEntity.Latitude = location.Latitude;
            datosEmpresaEntity.Longitude = location.Longitude;
            string datosEmpleado = "";

            datosEmpleado += "Nombre denunciante : " + model.DatosEmpleado.Nombre;
            datosEmpleado += Environment.NewLine;
            datosEmpleado += "Edad : " + model.DatosEmpleado.Edad;
            datosEmpleado += Environment.NewLine;
            datosEmpleado += "Email : " + model.DatosEmpleado.Email;
            datosEmpleado += Environment.NewLine;
            datosEmpleado += "Ocupacion : " + model.DatosEmpleado.Ocupacion;
            datosEmpleado += Environment.NewLine;
            datosEmpleado += "Ciudad : " + model.DatosEmpleado.Ciudad;
            datosEmpleado += Environment.NewLine;
            datosEmpleado += "Provincia : " + model.DatosEmpleado.Provincia;
            datosEmpleado += Environment.NewLine;
            datosEmpleado += "Telefono : " + model.DatosEmpleado.Telefono;
            datosEmpleado += Environment.NewLine;

            #region Fill entity
            var datosEmpleadoEntity = new DatosPersonales();
            datosEmpleadoEntity.NombreApellido = model.DatosEmpleado.Nombre;
            datosEmpleadoEntity.Edad = model.DatosEmpleado.Edad;
            datosEmpleadoEntity.DireccionE�Mail = model.DatosEmpleado.Email;
            datosEmpleadoEntity.Ocupacion = model.DatosEmpleado.Ocupacion;
            datosEmpleadoEntity.Ciudad = model.DatosEmpleado.Ciudad;
            datosEmpleadoEntity.Provincia = model.DatosEmpleado.Provincia;
            datosEmpleadoEntity.Telefono = model.DatosEmpleado.Telefono;

            #endregion

            infoDespido.DatoPersonal = datosEmpleadoEntity;
            infoDespido.DatoEmpresa = datosEmpresaEntity;


            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                unitOfWork.InfoDespidos.Add(infoDespido);
                unitOfWork.Complete();
            }
            var result = MailService.SendMail(model.DatosEmpleado.Nombre, mensaje + datosEmpresa + datosEmpleado, subject, model.DatosEmpleado.Email).Status;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["mail:contacto1"]))
            {

                var result1 = MailService.SendMail(model.DatosEmpleado.Nombre, mensaje + datosEmpresa + datosEmpleado, subject, model.DatosEmpleado.Email, ConfigurationManager.AppSettings["mail:contacto1"]).Status;
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["mail:contacto2"]))
            {

                var result1 = MailService.SendMail(model.DatosEmpleado.Nombre, mensaje + datosEmpresa + datosEmpleado, subject, model.DatosEmpleado.Email, ConfigurationManager.AppSettings["mail:contacto2"]).Status;
            }

            return Ok();
        }

        [HttpGet]
        [Route("GetLocations", Name = "GetLocations")]
        public async Task<IHttpActionResult> GetLocations()
        {
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {

                return Ok((await unitOfWork.DatosEmpresas.GetAllAsyncTask()).Select(u => this.TheModelFactory.Create(u)));

            }

        }


    }
}