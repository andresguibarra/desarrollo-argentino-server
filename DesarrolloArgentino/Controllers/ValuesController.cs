﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Persistence;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    //[Authorize]
    public class ValuesController : ApiController
    {
        // GET api/values
        public Noticia Get()
        {

            return new Noticia();

        }

        // GET api/values/5
        public Noticia Get(int id)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                return unitOfWork.Noticias.Get(id);
            }
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
