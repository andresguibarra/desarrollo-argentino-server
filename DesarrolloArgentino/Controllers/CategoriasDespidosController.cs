﻿using DesarrolloArgentino.Persistence;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    public class CategoriasDespidosController : BaseApiController
    {
        // get getall create

        public IHttpActionResult Get(long id)
        {
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var categoriaDespido = unitOfWork.CategoriasDespidos.Get(id);
                if (categoriaDespido != null)
                {
                    //return Ok(this.TheModelFactory.Create(categoriaDespido));
                }
                return NotFound();
            }
        }
    }
}