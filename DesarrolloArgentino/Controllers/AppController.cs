﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Models;
using DesarrolloArgentino.Persistence;
using DesarrolloArgentino.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    [RoutePrefix("api/app")]
    public class AppController : BaseApiController
    {
        //[HttpPost]
        //[HttpOptions]
        //[Route("SendMail", Name = "SendMail")]
        public async Task<IHttpActionResult> SendMail(SendMailBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await MailService.SendMail(model.Nombre, model.Mensaje, model.Asunto, model.MailFrom);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }

            return Ok();
        }
        [HttpPost]
        [HttpOptions]
        [Route("SendMail", Name = "SendMail")]
        public async Task<IHttpActionResult> SendMail(string nombre, string mensaje, string asuntos, string mailFrom)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            try
            {
                await MailService.SendMail(nombre, mensaje, asuntos, mailFrom);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }

            return Ok();
        }

        [HttpGet]
        [Route("GetPosts")]
        public async Task<IHttpActionResult> GetPosts(int length, int page = 1, int pageSize = 10)
        {

            var total = this.AppDbContext.Database.SqlQuery<int>("exec CountPosts").ToList().FirstOrDefault();



            var pageNumberP = new System.Data.SqlClient.SqlParameter { ParameterName = "page", Value = page };
            var rowspPageP = new System.Data.SqlClient.SqlParameter { ParameterName = "pageSize", Value = pageSize };

            var result = this.AppDbContext.Database.SqlQuery<GenericPostTSQL>("exec GetPosts @page, @pageSize", pageNumberP, rowspPageP);

            //var result = new PagedList<GenericPostTSQL>(genericPosts, page, pageSize);
            return Ok(new
            {
                TotalCount = total,

                Results = (await result.ToListAsync()).Select(u => this.TheModelFactory.Create(u, length))
            });
            //genericPosts.ToList();


        }

        [Authorize]
        [HttpGet]
        [Route("ItsLogedIn", Name = "ItsLogedIn")]
        public IHttpActionResult ItsLogedIn()
        {
            return Ok();
        }

        [HttpGet]
        [Route("Find")]
        public List<GenericPostReturnModel> Find(string parameter)
        {
            parameter = parameter.ToUpper();
            List<GenericPostReturnModel> list = new List<GenericPostReturnModel>();
            List<GenericPost> entityList = new List<GenericPost>();
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                entityList.AddRange(unitOfWork.Noticias.Find(c => c.Titulo.ToUpper().Contains(parameter)));
                entityList.AddRange(unitOfWork.Prensas.Find(c => c.Titulo.ToUpper().Contains(parameter)));
                entityList.AddRange(unitOfWork.Actividades.Find(c => c.Titulo.ToUpper().Contains(parameter)));

                entityList.AddRange(unitOfWork.Noticias.Find(c => c.Descripcion.ToUpper().Contains(parameter)));
                entityList.AddRange(unitOfWork.Prensas.Find(c => c.Descripcion.ToUpper().Contains(parameter)));
                entityList.AddRange(unitOfWork.Actividades.Find(c => c.Descripcion.ToUpper().Contains(parameter)));

                entityList.AddRange(unitOfWork.Noticias.Find(c => c.Autor.ToUpper().Contains(parameter)));
                entityList.AddRange(unitOfWork.Prensas.Find(c => c.Autor.ToUpper().Contains(parameter)));
                entityList.AddRange(unitOfWork.Actividades.Find(c => c.Autor.ToUpper().Contains(parameter)));

                list.AddRange(entityList.Select(c => TheModelFactory.Create(c, 200)));
                list = list.Distinct(new Comparer()).ToList();
            }
            return list;
        }

        class Comparer : IEqualityComparer<GenericPostReturnModel>
        {
            public bool Equals(GenericPostReturnModel x, GenericPostReturnModel y)
            {
                if (Object.ReferenceEquals(x, y)) return true;
                if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) return false;
                return x.Id == y.Id && x.Categoria.Equals(y.Categoria);
            }

            public int GetHashCode(GenericPostReturnModel obj)
            {
                if (Object.ReferenceEquals(obj, null)) return 0;
                int hashProductName = obj.Categoria?.GetHashCode() ?? 0;
                int hashProductCode = obj.Id.GetHashCode();
                return hashProductName ^ hashProductCode;
            }
        }
    }
}
