﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Models;
using DesarrolloArgentino.Persistence;
using DesarrolloArgentino.Services;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    [RoutePrefix("api/actividades")]
    public class ActividadesController : BaseApiController
    {
        [Route("{id:long}", Name = "GetActividadById")]
        public IHttpActionResult Get(long id)
        {
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var actividad = unitOfWork.Actividades.Get(id);
                if (actividad != null)
                {
                    return Ok(this.TheModelFactory.Create(actividad));
                }
                return NotFound();
            }
        }

        public async Task<IHttpActionResult> Get()
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                return Ok((await unitOfWork.Actividades.GetAllAsyncTask()).Select(n => this.TheModelFactory.Create(n)));
            }
        }
        [Authorize]
        [Route("Create")]
        public IHttpActionResult Create(CreateActividadBindingModel model)
        {

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model invalid");
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var entity = this.TheModelFactory.Parse(model);
                var singleOrDefault = unitOfWork.Categorias.Find(c => c.Nombre == "Actividades").SingleOrDefault();
                if (singleOrDefault != null)
                    entity.CategoriaId = singleOrDefault.CategoriaId;
                unitOfWork.Actividades.Add(entity);
                var locationHeader = new Uri(Url.Link("GetActividadById", new { id = entity.ActividadId }));

                unitOfWork.Complete();

                return Created(locationHeader, this.TheModelFactory.Create(entity));
            }

        }

        [Authorize]
        [Route("Update")]
        public IHttpActionResult Update(UpdateActividadBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model invalid");
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var categoria = unitOfWork.Categorias.Find(c => c.Nombre == model.CategoriaNombre).First();
                var entity = unitOfWork.Actividades.Get(model.Id);
                if (entity == null)
                {
                    ModelState.AddModelError("", "null model");
                    return BadRequest(ModelState);
                }
                entity.ImageUrl = model.ImageUrl;
                entity.Autor = model.Autor;
                entity.Contenido = model.Contenido;
                entity.Descripcion = model.Descripcion;
                entity.FechaUltimaModificacion = DateTime.Now;
                entity.Titulo = model.Titulo;

                if (entity.CategoriaId == categoria.CategoriaId)
                {
                    var locationHeader = new Uri(Url.Link("GetActividadById", new { id = entity.ActividadId }));
                    unitOfWork.Complete();

                    return Created(locationHeader, TheModelFactory.Create(entity));
                }
                if (categoria.Nombre.ToUpper().Equals("NOTICIAS"))
                {
                    var newEntity = new Noticia
                    {
                        ImageUrl = entity.ImageUrl,
                        Autor = entity.Autor,
                        Contenido = entity.Contenido,
                        Descripcion = entity.Descripcion,
                        FechaUltimaModificacion = DateTime.Now,
                        Titulo = entity.Titulo,
                        CategoriaId = categoria.CategoriaId,
                        FechaCreacion = entity.FechaCreacion
                    };

                    unitOfWork.Actividades.Remove(entity);
                    unitOfWork.Noticias.Add(newEntity);

                    var locationHeader = new Uri(Url.Link("GetNoticiaById", new { id = newEntity.NoticiaId }));
                    unitOfWork.Complete();

                    return Created(locationHeader, TheModelFactory.Create(newEntity));
                }
                if (categoria.Nombre.ToUpper().Equals("PRENSA"))
                {
                    var newEntity = new Prensa
                    {
                        ImageUrl = entity.ImageUrl,
                        Autor = entity.Autor,
                        Contenido = entity.Contenido,
                        Descripcion = entity.Descripcion,
                        FechaUltimaModificacion = DateTime.Now,
                        Titulo = entity.Titulo,
                        CategoriaId = categoria.CategoriaId,
                        FechaCreacion = entity.FechaCreacion
                    };

                    unitOfWork.Actividades.Remove(entity);
                    unitOfWork.Prensas.Add(newEntity);
                    var locationHeader = new Uri(Url.Link("GetPrensaById", new { id = newEntity.PrensaId }));
                    unitOfWork.Complete();

                    return Created(locationHeader, TheModelFactory.Create(newEntity));
                }
                return BadRequest();
            }
        }

        #region UploadImages
        [Authorize]
        [Route("uploadImages")]
        public async Task<IHttpActionResult> UploadImages()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                //throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                ModelState.AddModelError("", "No se pudo guardar la imagen en la base de datos");
                return BadRequest(ModelState);
            }

            string root = HttpContext.Current.Server.MapPath("~/images");
            var provider = new MultipartFormDataStreamProvider(root);
            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);

                var id = provider.FormData["id"] == "" ? 0 : long.Parse(provider.FormData["id"]);

                using (var unitOfWork = new UnitOfWork(this.AppDbContext))
                {
                    var entity = unitOfWork.Actividades.Get(id);
                    if (entity != null)
                    {
                        entity.ImageUrl = ImageService.UploadImage(provider);
                        unitOfWork.Complete();

                    }
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                return BadRequest(ModelState);
            }
            return Ok();

        }
        #endregion
        [HttpPost]
        [Authorize]
        [Route("delete/{id:long}", Name = "DeleteActividadById")]
        public IHttpActionResult Delete(long id)
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var actividad = unitOfWork.Actividades.Get(id);
                if (actividad != null)
                {
                    unitOfWork.Actividades.Remove(actividad);
                    unitOfWork.Complete();
                    return Ok();
                }
                return NotFound();
            }

        }
        public async Task<IHttpActionResult> GetMini(int length, bool paging = true, int page = 1, int pageSize = ItemsPerPage)
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {

                //return Ok((await unitOfWork.Noticias.GetAllAsyncTask()).OrderByDescending(n => n.NoticiaId).Take(page).Select(n => TheModelFactory.Create(n, length)));
                var items = unitOfWork.Actividades.GetAllAsQueryable();
                var result = new PagedList<Actividad>(items.OrderByDescending(n => n.ActividadId), page, pageSize);
                return Ok(new
                {
                    TotalCount = result.TotalItemCount,
                    TotalPages = result.TotalPages,
                    HasPreviousPage = result.HasPreviousPage,
                    HasNextPage = result.HasNextPage,
                    Results = (await result.Items.ToListAsync()).Select(u => this.TheModelFactory.Create(u, length))
                });


                //return Ok((await unitOfWork.Noticias.GetAllAsyncTask()).Select(n => TheModelFactory.Create(n, length)));
            }

        }
    }
}
