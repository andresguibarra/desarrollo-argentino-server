﻿using DesarrolloArgentino.Models;
using DesarrolloArgentino.Persistence;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Http;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    public class BaseApiController : ApiController
    {

        private ModelFactory _modelFactory;
        private DesarrolloArgentinoContext _desarrolloArgentinoContext;
        private readonly ApplicationUserManager _appUserManager = null;
        protected const int ItemsPerPage = 10;

        protected DesarrolloArgentinoContext AppDbContext => _desarrolloArgentinoContext ?? Request.GetOwinContext().Get<DesarrolloArgentinoContext>();


        protected ApplicationUserManager AppUserManager => _appUserManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();



        protected ModelFactory TheModelFactory
        {
            get
            {
                if (_modelFactory == null)
                {
                    _modelFactory = new ModelFactory(this.Request, this.AppUserManager, this.AppDbContext);
                }
                return _modelFactory;
            }
        }

        protected DesarrolloArgentinoContext.ApplicationUser GetAuthenticatedUser()
        {
            DesarrolloArgentinoContext.ApplicationUser user = null;

            try
            {
                user = this.AppUserManager.FindById(User.Identity.GetUserId());
            }
            catch { }

            return user;
        }
        protected IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
