﻿using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Models;
using DesarrolloArgentino.Persistence;
using DesarrolloArgentino.Services;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace DesarrolloArgentino.Controllers
{
    [RoutePrefix("api/prensas")]
    public class PrensasController : BaseApiController
    {
        [Route("{id:long}", Name = "GetprensaById")]
        public IHttpActionResult Get(long id)
        {
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var prensa = unitOfWork.Prensas.Get(id);
                if (prensa != null)
                {
                    return Ok(this.TheModelFactory.Create(prensa));
                }
                return NotFound();
            }
        }

        public async Task<IHttpActionResult> Get()
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                return Ok((await unitOfWork.Prensas.GetAllAsyncTask()).Select(n => this.TheModelFactory.Create(n)));
            }
        }
        [Authorize]
        [Route("Create")]
        public IHttpActionResult Create(CreatePrensaBindingModel model)
        {

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model invalid");
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var entity = this.TheModelFactory.Parse(model);
                var singleOrDefault = unitOfWork.Categorias.Find(c => c.Nombre == "Prensa").SingleOrDefault();
                if (singleOrDefault != null)
                    entity.CategoriaId = singleOrDefault.CategoriaId;
                unitOfWork.Prensas.Add(entity);
                var locationHeader = new Uri(Url.Link("GetPrensaById", new { id = entity.PrensaId }));

                unitOfWork.Complete();

                return Created(locationHeader, this.TheModelFactory.Create(entity));
            }

        }

        [Authorize]
        [Route("Update")]
        public IHttpActionResult Update(UpdatePrensaBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("", "Model invalid");
                return BadRequest(ModelState);
            }
            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var categoria = unitOfWork.Categorias.Find(c => c.Nombre == model.CategoriaNombre).First();
                var entity = unitOfWork.Prensas.Get(model.Id);
                if (entity == null)
                {
                    ModelState.AddModelError("", "null model");
                    return BadRequest(ModelState);
                }
                entity.ImageUrl = model.ImageUrl;
                entity.Autor = model.Autor;
                entity.Contenido = model.Contenido;
                entity.Descripcion = model.Descripcion;
                entity.FechaUltimaModificacion = DateTime.Now;
                entity.Titulo = model.Titulo;

                if (entity.CategoriaId == categoria.CategoriaId)
                {
                    var locationHeader = new Uri(Url.Link("GetPrensaById", new { id = entity.PrensaId }));
                    unitOfWork.Complete();

                    return Created(locationHeader, TheModelFactory.Create(entity));
                }
                if (categoria.Nombre.ToUpper().Equals("NOTICIAS"))
                {
                    var newEntity = new Noticia
                    {
                        ImageUrl = entity.ImageUrl,
                        Autor = entity.Autor,
                        Contenido = entity.Contenido,
                        Descripcion = entity.Descripcion,
                        FechaUltimaModificacion = DateTime.Now,
                        Titulo = entity.Titulo,
                        CategoriaId = categoria.CategoriaId,
                        FechaCreacion = entity.FechaCreacion
                    };

                    unitOfWork.Prensas.Remove(entity);
                    unitOfWork.Noticias.Add(newEntity);

                    var locationHeader = new Uri(Url.Link("GetNoticiaById", new { id = newEntity.NoticiaId }));
                    unitOfWork.Complete();

                    return Created(locationHeader, TheModelFactory.Create(newEntity));
                }
                if (categoria.Nombre.ToUpper().Equals("ACTIVIDADES"))
                {
                    var newEntity = new Actividad
                    {
                        ImageUrl = entity.ImageUrl,
                        Autor = entity.Autor,
                        Contenido = entity.Contenido,
                        Descripcion = entity.Descripcion,
                        FechaUltimaModificacion = DateTime.Now,
                        Titulo = entity.Titulo,
                        CategoriaId = categoria.CategoriaId,
                        FechaCreacion = entity.FechaCreacion
                    };

                    unitOfWork.Prensas.Remove(entity);
                    unitOfWork.Actividades.Add(newEntity);

                    var locationHeader = new Uri(Url.Link("GetActividadById", new { id = newEntity.ActividadId }));
                    unitOfWork.Complete();

                    return Created(locationHeader, TheModelFactory.Create(newEntity));
                }

                return BadRequest();
            }
        }
        #region UploadImages
        [Authorize]
        [Route("uploadImages")]
        public async Task<IHttpActionResult> UploadImages()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                //throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                ModelState.AddModelError("", "No se pudo guardar la imagen en la base de datos");
                return BadRequest(ModelState);
            }

            GenericPost genericPost = new Noticia();
            string root = HttpContext.Current.Server.MapPath("~/images");
            var provider = new MultipartFormDataStreamProvider(root);
            try
            {
                await Request.Content.ReadAsMultipartAsync(provider);

                var id = provider.FormData["id"] == "" ? 0 : long.Parse(provider.FormData["id"]);

                using (var unitOfWork = new UnitOfWork(this.AppDbContext))
                {
                    var entity = unitOfWork.Prensas.Get(id);
                    if (entity != null)
                    {
                        entity.ImageUrl = ImageService.UploadImage(provider);
                        unitOfWork.Complete();

                    }
                }

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", ex.Message);

                return BadRequest(ModelState);
            }
            return Ok();

        }
        #endregion
        [HttpPost]
        [Authorize]
        [Route("delete/{id:long}", Name = "DeletePrensaById")]
        public IHttpActionResult Delete(long id)
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {
                var prensa = unitOfWork.Prensas.Get(id);
                if (prensa != null)
                {
                    unitOfWork.Prensas.Remove(prensa);
                    unitOfWork.Complete();
                    return Ok();
                }
                return NotFound();
            }

        }

        public async Task<IHttpActionResult> GetMini(int length, bool paging = true, int page = 1, int pageSize = ItemsPerPage)
        {

            using (var unitOfWork = new UnitOfWork(this.AppDbContext))
            {

                //return Ok((await unitOfWork.Noticias.GetAllAsyncTask()).OrderByDescending(n => n.NoticiaId).Take(page).Select(n => TheModelFactory.Create(n, length)));
                var items = unitOfWork.Prensas.GetAllAsQueryable();
                var result = new PagedList<Prensa>(items.OrderByDescending(n => n.PrensaId), page, pageSize);
                return Ok(new
                {
                    TotalCount = result.TotalItemCount,
                    TotalPages = result.TotalPages,
                    HasPreviousPage = result.HasPreviousPage,
                    HasNextPage = result.HasNextPage,
                    Results = (await result.Items.ToListAsync()).Select(u => this.TheModelFactory.Create(u, length))
                });


                //return Ok((await unitOfWork.Noticias.GetAllAsyncTask()).Select(n => TheModelFactory.Create(n, length)));
            }

        }
    }
}
