﻿using DesarrolloArgentino.Core;
using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Persistence;
using System;
using System.Net.Http;
using System.Web.Http.Routing;

namespace DesarrolloArgentino.Models
{
    public class ModelFactory
    {
        private UrlHelper _urlHelper;
        private readonly DesarrolloArgentinoContext _appDbContext;
        private ApplicationUserManager _appUserManager;
        private HttpRequestMessage _request;
        private IUnitOfWork _unitOfWork;

        public ModelFactory(HttpRequestMessage request, ApplicationUserManager appUserManager, DesarrolloArgentinoContext appDbContext)
        {
            _urlHelper = new UrlHelper(request);
            this._request = request;
            this._appUserManager = appUserManager;
            this._appDbContext = appDbContext;
            this._unitOfWork = new UnitOfWork(appDbContext);

        }





        public NoticiaReturnModel Create(Noticia noticia)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                return new NoticiaReturnModel()
                {
                    Id = noticia.NoticiaId,
                    Descripcion = noticia.Descripcion,
                    Contenido = noticia.Contenido,
                    Autor = noticia.Autor,
                    Titulo = noticia.Titulo,
                    Categoria = unitOfWork.Categorias.Get(noticia.CategoriaId).Nombre,
                    ImageUrl = noticia.ImageUrl,
                    FechaCreacion = noticia.FechaCreacion,
                    FechaUltimaModificacion = noticia.FechaUltimaModificacion

                };
            }

        }
        public NoticiaReturnModel Create(Noticia noticia, int length)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                //var cate = _unitOfWork.Categorias.Get(noticia.CategoriaId).Nombre;
                return new NoticiaReturnModel()
                {
                    Id = noticia.NoticiaId,
                    Descripcion = noticia.Descripcion,
                    Contenido = noticia.Contenido.Length > length ? noticia.Contenido.Substring(0, length) + "..." : noticia.Contenido,
                    Autor = noticia.Autor, //TODO agregar autor
                    Titulo = noticia.Titulo,
                    Categoria = unitOfWork.Categorias.Get(noticia.CategoriaId).Nombre,
                    ImageUrl = noticia.ImageUrl,
                    FechaCreacion = noticia.FechaCreacion,
                    FechaUltimaModificacion = noticia.FechaUltimaModificacion


                };
            }

        }

        public ActividadReturnModel Create(Actividad actividad)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                return new ActividadReturnModel()
                {
                    Id = actividad.ActividadId,
                    Descripcion = actividad.Descripcion,
                    Contenido = actividad.Contenido,
                    Autor = actividad.Autor,
                    Titulo = actividad.Titulo,
                    Categoria = unitOfWork.Categorias.Get(actividad.CategoriaId).Nombre,
                    FechaCreacion = actividad.FechaCreacion,
                    FechaUltimaModificacion = actividad.FechaUltimaModificacion,
                    ImageUrl = actividad.ImageUrl
                };
            }
        }
        public ActividadReturnModel Create(Actividad actividad, int length)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                //var cate = _unitOfWork.Categorias.Get(actividad.CategoriaId).Nombre;
                return new ActividadReturnModel()
                {
                    Id = actividad.ActividadId,
                    Descripcion = actividad.Descripcion,
                    Contenido = actividad.Contenido.Length > length ? actividad.Contenido.Substring(0, length) + "..." : actividad.Contenido,
                    Autor = actividad.Autor,
                    Titulo = actividad.Titulo,
                    Categoria = unitOfWork.Categorias.Get(actividad.CategoriaId).Nombre,
                    ImageUrl = actividad.ImageUrl,
                    FechaCreacion = actividad.FechaCreacion,
                    FechaUltimaModificacion = actividad.FechaUltimaModificacion


                };
            }

        }

        public PrensaReturnModel Create(Prensa prensa, int length)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                //var cate = _unitOfWork.Categorias.Get(prensa.CategoriaId).Nombre;
                return new PrensaReturnModel()
                {
                    Id = prensa.PrensaId,
                    Descripcion = prensa.Descripcion,
                    Contenido = prensa.Contenido.Length > length ? prensa.Contenido.Substring(0, length) + "..." : prensa.Contenido,
                    Autor = prensa.Autor,
                    Titulo = prensa.Titulo,
                    Categoria = unitOfWork.Categorias.Get(prensa.CategoriaId).Nombre,
                    ImageUrl = prensa.ImageUrl,
                    FechaCreacion = prensa.FechaCreacion,
                    FechaUltimaModificacion = prensa.FechaUltimaModificacion


                };
            }

        }
        public PrensaReturnModel Create(Prensa prensa)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                return new PrensaReturnModel()
                {
                    Id = prensa.PrensaId,
                    Descripcion = prensa.Descripcion,
                    Contenido = prensa.Contenido,
                    Autor = prensa.Autor,
                    Titulo = prensa.Titulo,
                    Categoria = unitOfWork.Categorias.Get(prensa.CategoriaId).Nombre,
                    ImageUrl = prensa.ImageUrl,
                    FechaCreacion = prensa.FechaCreacion,
                    FechaUltimaModificacion = prensa.FechaUltimaModificacion


                };
            }

        }

        public InfoDespidoReturnModel Create(InfoDespido infoDespido)
        {
            using (var unitofWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {
                return new InfoDespidoReturnModel()
                {
                    InfoDespidoId = infoDespido.InfoDespidoId,
                    CategoriaDespidoId = infoDespido.InfoDespidoId,
                    Comentarios = infoDespido.Comentarios,
                    DatosEmpresaId = infoDespido.DatosEmpresaId,
                    DatosPersonalesId = infoDespido.DatosPersonalesId,
                    FuncionEmpresa = infoDespido.FuncionEmpresa,
                    //OpcionesId = infoDespido.OpcionesId
                };
            }
        }

        public GenericPostReturnModel Create(GenericPost entity, int length)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                //var cate = _unitOfWork.Categorias.Get(actividad.CategoriaId).Nombre;
                GenericPostReturnModel retorno = new GenericPostReturnModel();
                long id;
                if (entity is Actividad)
                    id = ((Actividad)entity).ActividadId;
                else if (entity is Noticia)
                    id = ((Noticia)entity).NoticiaId;
                else
                    id = ((Prensa)entity).PrensaId;
                return new GenericPostReturnModel()
                {
                    Id = id,
                    Descripcion = entity.Descripcion,
                    Contenido = entity.Contenido.Length > length ? entity.Contenido.Substring(0, length) + "..." : entity.Contenido,
                    Autor = entity.Autor,
                    Titulo = entity.Titulo,
                    Categoria = unitOfWork.Categorias.Get(entity.CategoriaId).Nombre,
                    ImageUrl = entity.ImageUrl,
                    FechaCreacion = entity.FechaCreacion,
                    FechaUltimaModificacion = entity.FechaUltimaModificacion


                };
            }

        }

        public GenericPostReturnModel Create(GenericPostTSQL entity, int length)
        {
            using (var unitOfWork = new UnitOfWork(DesarrolloArgentinoContext.Create()))
            {

                //var cate = _unitOfWork.Categorias.Get(actividad.CategoriaId).Nombre;

                return new GenericPostReturnModel()
                {
                    Id = entity.Id,
                    Descripcion = entity.Descripcion,
                    Contenido = entity.Contenido.Length > length ? entity.Contenido.Substring(0, length) + "..." : entity.Contenido,
                    Autor = entity.Autor,
                    Titulo = entity.Titulo,
                    Categoria = unitOfWork.Categorias.Get(entity.CategoriaId).Nombre,
                    ImageUrl = entity.ImageUrl,
                    FechaCreacion = entity.FechaCreacion,
                    FechaUltimaModificacion = entity.FechaUltimaModificacion


                };
            }

        }

        public Noticia Parse(CreateNoticiaBindingModel model)
        {


            return new Noticia()
            {
                Titulo = model.Titulo,
                Contenido = model.Contenido,
                Autor = model.Autor,
                Descripcion = model.Descripcion,
                FechaCreacion = DateTime.Now,
                FechaUltimaModificacion = DateTime.Now,
                ImageUrl = model.ImageUrl


            };

        }

        public Actividad Parse(CreateActividadBindingModel model)
        {


            return new Actividad()
            {
                Titulo = model.Titulo,
                Contenido = model.Contenido,
                Autor = model.Autor,
                Descripcion = model.Descripcion,
                FechaCreacion = DateTime.Now,
                FechaUltimaModificacion = DateTime.Now,
                ImageUrl = model.ImageUrl

            };

        }

        public Prensa Parse(CreatePrensaBindingModel model)
        {


            return new Prensa()
            {
                Titulo = model.Titulo,
                Contenido = model.Contenido,
                Autor = model.Autor,
                Descripcion = model.Descripcion,

                FechaCreacion = DateTime.Now,
                FechaUltimaModificacion = DateTime.Now,
                ImageUrl = model.ImageUrl

            };

        }

        public InfoDespido Parse(CreateInfoDespidoBindingModel model)
        {
            return new InfoDespido()
            {
                //InfoDespidoId = model.InfoDespidoId,
                //DatosEmpresaId = model.DatosEmpresaId,
                //DatosPersonalesId = model.DatosPersonalesId,
                CategoriaDespidoId = model.CategoriaDespidoId,
                OpcionesId = model.IdOpcionElegida,
                Comentarios = model.Descripcion,
                FuncionEmpresa = model.Funcion
            };
        }

        public LocationModel Create(DatosEmpresa empresa)
        {
            return new LocationModel()
            {
                Longitude = empresa.Longitude,
                Latitude = empresa.Latitude,
                NombreEmpresa = empresa.NombreDeEmpresa
            };
        }
    }

    public class InfoDespidoReturnModel
    {
        public long InfoDespidoId { get; set; }
        public long CategoriaDespidoId { get; set; }
        public long DatosEmpresaId { get; set; }
        public long DatosPersonalesId { get; set; }
        public long OpcionesId { get; set; }
        public string Comentarios { get; set; }
        public string FuncionEmpresa { get; set; }

    }

    public class ActividadReturnModel : GenericPostReturnModel
    {

    }

    public class NoticiaReturnModel : GenericPostReturnModel
    {


    }

    public class PrensaReturnModel : GenericPostReturnModel
    {

    }

    public enum CATEGORIAS : long
    {
        Noticia = (long)1L,
        Actividad = 2L,
        Prensa = 3L
    }

    public class GenericPostReturnModel
    {
        public long Id { get; set; }

        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Contenido { get; set; }
        public string Autor { get; set; }
        public string Categoria { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? FechaCreacion { get; set; }
        public DateTime? FechaUltimaModificacion { get; set; }
    }
}