﻿using System.ComponentModel.DataAnnotations;

namespace DesarrolloArgentino.Models
{
    public class SendMailBindingModel
    {

        [Required]
        public string Nombre { get; set; }
        [Required]
        public string MailFrom { get; set; }
        public string Asunto { get; set; }

        [Required]
        public string Mensaje { get; set; }
    }

    public abstract class CreateGenericPostBindingModel
    {
        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public string Contenido { get; set; }

        public string Autor { get; set; }

        public string ImageUrl { get; set; }
    }

    public class CreateInfoDespidoBindingModel
    {
        public long CategoriaDespidoId { get; set; }
        public long IdOpcionElegida { get; set; }
        public string Descripcion { get; set; }
        public string Funcion { get; set; }

        public CreateDatosEmpresa DatosEmpresa { get; set; }
        public CreateDatosEmpleado DatosEmpleado { get; set; }
    }

    public class CreateDatosEmpleado
    {
        public string Ciudad { get; set; }

        public long? Edad { get; set; }
        public string Ocupacion { get; set; }
        public string Nombre { get; set; }
        public string Email { get; set; }
        public string Provincia { get; set; }
        public string Telefono { get; set; }
    }

    public class CreateDatosEmpresa
    {
        public long? CantidadEmpleados { get; set; }

        public string Ciudad { get; set; }
        public string Direccion { get; set; }
        public string Nombre { get; set; }
        public string Provincia { get; set; }
        public string Rubro { get; set; }


    }

    public class CreateNoticiaBindingModel : CreateGenericPostBindingModel
    {

    }

    public class CreatePrensaBindingModel : CreateGenericPostBindingModel
    {

    }
    public class CreateActividadBindingModel : CreateGenericPostBindingModel
    {

    }

    public abstract class UpdateGenericPostBindingModel
    {
        public long Id { get; set; }

        public string CategoriaNombre { get; set; }

        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public string Contenido { get; set; }

        public string Autor { get; set; }

        public string ImageUrl { get; set; }
    }

    public class UpdateNoticiaBindingModel : UpdateGenericPostBindingModel
    {

    }

    public class UpdatePrensaBindingModel : UpdateGenericPostBindingModel
    {

    }
    public class UpdateActividadBindingModel : UpdateGenericPostBindingModel
    {

    }
}