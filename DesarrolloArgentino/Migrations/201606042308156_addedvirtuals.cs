namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedvirtuals : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.InfoDespidoes", "CategoriaDespidoId", c => c.Long(nullable: false));
            CreateIndex("dbo.InfoDespidoes", "CategoriaDespidoId");
            CreateIndex("dbo.InfoDespidoes", "DatosEmpresaId");
            CreateIndex("dbo.InfoDespidoes", "DatosPersonalesId");
            CreateIndex("dbo.InfoDespidoes", "OpcionesId");
            AddForeignKey("dbo.InfoDespidoes", "CategoriaDespidoId", "dbo.CategoriaDespidoes", "CategoriaDespidoId", cascadeDelete: true);
            AddForeignKey("dbo.InfoDespidoes", "DatosEmpresaId", "dbo.DatosEmpresas", "DatosEmpresaId", cascadeDelete: true);
            AddForeignKey("dbo.InfoDespidoes", "DatosPersonalesId", "dbo.DatosPersonales", "DatosPersonalesId", cascadeDelete: true);
            AddForeignKey("dbo.InfoDespidoes", "OpcionesId", "dbo.Opciones", "OpcionesId", cascadeDelete: true);
            DropColumn("dbo.InfoDespidoes", "CategoriaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.InfoDespidoes", "CategoriaId", c => c.Long(nullable: false));
            DropForeignKey("dbo.InfoDespidoes", "OpcionesId", "dbo.Opciones");
            DropForeignKey("dbo.InfoDespidoes", "DatosPersonalesId", "dbo.DatosPersonales");
            DropForeignKey("dbo.InfoDespidoes", "DatosEmpresaId", "dbo.DatosEmpresas");
            DropForeignKey("dbo.InfoDespidoes", "CategoriaDespidoId", "dbo.CategoriaDespidoes");
            DropIndex("dbo.InfoDespidoes", new[] { "OpcionesId" });
            DropIndex("dbo.InfoDespidoes", new[] { "DatosPersonalesId" });
            DropIndex("dbo.InfoDespidoes", new[] { "DatosEmpresaId" });
            DropIndex("dbo.InfoDespidoes", new[] { "CategoriaDespidoId" });
            DropColumn("dbo.InfoDespidoes", "CategoriaDespidoId");
        }
    }
}
