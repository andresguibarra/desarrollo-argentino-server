namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeFromLongToDoubleLocation : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DatosEmpresas", "Latitude", c => c.Double(nullable: false));
            AlterColumn("dbo.DatosEmpresas", "Longitude", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.DatosEmpresas", "Longitude", c => c.Long(nullable: false));
            AlterColumn("dbo.DatosEmpresas", "Latitude", c => c.Long(nullable: false));
        }
    }
}
