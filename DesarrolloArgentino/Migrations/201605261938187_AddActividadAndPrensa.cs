namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddActividadAndPrensa : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Actividades",
                c => new
                    {
                        ActividadId = c.Long(nullable: false, identity: true),
                        Titulo = c.String(),
                        Descripcion = c.String(),
                        Autor = c.String(),
                        Contenido = c.String(),
                        ImageUrl = c.String(),
                        CategoriaId = c.Long(nullable: false),
                        FechaCreacion = c.DateTime(),
                        FechaUltimaModificacion = c.DateTime(),
                    })
                .PrimaryKey(t => t.ActividadId)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.CategoriaId);
            
            CreateTable(
                "dbo.Prensas",
                c => new
                    {
                        PrensaId = c.Long(nullable: false, identity: true),
                        Titulo = c.String(),
                        Descripcion = c.String(),
                        Autor = c.String(),
                        Contenido = c.String(),
                        ImageUrl = c.String(),
                        CategoriaId = c.Long(nullable: false),
                        FechaCreacion = c.DateTime(),
                        FechaUltimaModificacion = c.DateTime(),
                    })
                .PrimaryKey(t => t.PrensaId)
                .ForeignKey("dbo.Categorias", t => t.CategoriaId, cascadeDelete: true)
                .Index(t => t.CategoriaId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Prensas", "CategoriaId", "dbo.Categorias");
            DropForeignKey("dbo.Actividades", "CategoriaId", "dbo.Categorias");
            DropIndex("dbo.Prensas", new[] { "CategoriaId" });
            DropIndex("dbo.Actividades", new[] { "CategoriaId" });
            DropTable("dbo.Prensas");
            DropTable("dbo.Actividades");
        }
    }
}
