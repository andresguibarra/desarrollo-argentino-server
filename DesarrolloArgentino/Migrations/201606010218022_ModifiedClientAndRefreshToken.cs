namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedClientAndRefreshToken : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RefreshTokens",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Subject = c.String(nullable: false, maxLength: 50),
                        ClientId = c.String(nullable: false, maxLength: 50),
                        IssuedUtc = c.DateTime(nullable: false),
                        ExpiresUtc = c.DateTime(nullable: false),
                        ProtectedTicket = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Clients", "Secret", c => c.String(nullable: false));
            AddColumn("dbo.Clients", "ApplicationType", c => c.Int(nullable: false));
            AddColumn("dbo.Clients", "Active", c => c.Boolean(nullable: false));
            AddColumn("dbo.Clients", "RefreshTokenLifeTime", c => c.Int(nullable: false));
            AddColumn("dbo.Clients", "AllowedOrigin", c => c.String(maxLength: 100));
            AlterColumn("dbo.Clients", "Name", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.Clients", "ClientSecretHash");
            DropColumn("dbo.Clients", "AllowedGrant");
            DropColumn("dbo.Clients", "CreatedOn");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Clients", "CreatedOn", c => c.DateTimeOffset(nullable: false, precision: 7));
            AddColumn("dbo.Clients", "AllowedGrant", c => c.Int(nullable: false));
            AddColumn("dbo.Clients", "ClientSecretHash", c => c.String());
            AlterColumn("dbo.Clients", "Name", c => c.String());
            DropColumn("dbo.Clients", "AllowedOrigin");
            DropColumn("dbo.Clients", "RefreshTokenLifeTime");
            DropColumn("dbo.Clients", "Active");
            DropColumn("dbo.Clients", "ApplicationType");
            DropColumn("dbo.Clients", "Secret");
            DropTable("dbo.RefreshTokens");
        }
    }
}
