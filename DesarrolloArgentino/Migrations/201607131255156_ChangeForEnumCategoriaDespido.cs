namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeForEnumCategoriaDespido : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InfoDespidoes", "CategoriaDespidoId", "dbo.CategoriaDespidoes");
            DropIndex("dbo.InfoDespidoes", new[] { "CategoriaDespidoId" });
            AddColumn("dbo.InfoDespidoes", "CategoriaDespido", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.InfoDespidoes", "CategoriaDespido");
            CreateIndex("dbo.InfoDespidoes", "CategoriaDespidoId");
            AddForeignKey("dbo.InfoDespidoes", "CategoriaDespidoId", "dbo.CategoriaDespidoes", "CategoriaDespidoId", cascadeDelete: true);
        }
    }
}
