namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCategoriaAndChangedNoticia : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Images", "Noticia_NoticiaId", "dbo.Noticias");
            DropIndex("dbo.Images", new[] { "Noticia_NoticiaId" });
            CreateTable(
                "dbo.Categorias",
                c => new
                    {
                        CategoriaId = c.Long(nullable: false, identity: true),
                        Nombre = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.CategoriaId);
            
            AddColumn("dbo.Noticias", "ImageUrl", c => c.String());
            AddColumn("dbo.Noticias", "FechaCreacion", c => c.DateTime(nullable: false));
            AddColumn("dbo.Noticias", "FechaUltimaModificacion", c => c.DateTime(nullable: false));
            DropColumn("dbo.Images", "Noticia_NoticiaId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Images", "Noticia_NoticiaId", c => c.Long());
            DropColumn("dbo.Noticias", "FechaUltimaModificacion");
            DropColumn("dbo.Noticias", "FechaCreacion");
            DropColumn("dbo.Noticias", "ImageUrl");
            DropTable("dbo.Categorias");
            CreateIndex("dbo.Images", "Noticia_NoticiaId");
            AddForeignKey("dbo.Images", "Noticia_NoticiaId", "dbo.Noticias", "NoticiaId");
        }
    }
}
