namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddForeignKeyCategoria : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Noticias", "CategoriaId", c => c.Long(nullable: false));
            CreateIndex("dbo.Noticias", "CategoriaId");
            AddForeignKey("dbo.Noticias", "CategoriaId", "dbo.Categorias", "CategoriaId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Noticias", "CategoriaId", "dbo.Categorias");
            DropIndex("dbo.Noticias", new[] { "CategoriaId" });
            DropColumn("dbo.Noticias", "CategoriaId");
        }
    }
}
