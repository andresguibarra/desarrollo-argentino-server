using DesarrolloArgentino.Core.Entities;
using DesarrolloArgentino.Providers;

namespace DesarrolloArgentino.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DesarrolloArgentino.Persistence.DesarrolloArgentinoContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DesarrolloArgentino.Persistence.DesarrolloArgentinoContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //


            //context.Categorias.AddOrUpdate(
            //    new Categoria
            //    {
            //        CategoriaId = 1,
            //        Nombre = "Noticias"
            //    },
            //    new Categoria
            //    {
            //        CategoriaId = 2,
            //        Nombre = "Actividades"
            //    },
            //      new Categoria
            //      {
            //          CategoriaId = 3,
            //          Nombre = "Prensa"
            //      }
            //    );

            //context.Noticias.AddOrUpdate(
            //  new Noticia
            //  {
            //      Titulo = "Presentaci�n del libro-DIALOGO LATINOAMERICANO-DAR",
            //      Descripcion = "Description",
            //      CategoriaId = 1,
            //      ImageUrl = "",
            //      Contenido = "El mi�rcoles 14 de octubre se present� Di�logo Latinoamericano / Apertura Argentina, un libro que re�ne los aportes de m�s de veinte reconocidos intelectuales, acad�micos y artistas provenientes de todas las regiones de la Argentina. Asumiendo su condici�n de latinoamericanos que nacieron y viven en la Argentina, se lanzaron a una construcci�n colectiva en la que se exponen desarrollos te�ricos, estudios sobre pensadores de Nuestra Am�rica y propuestas para la acci�n que distan de ser uniformes, pero -precisamente por esa falta de uniformidad- se enriquecen mutuamente.",
            //      NoticiaId = 1,
            //      //FechaUltimaModificacion = new DateTime(),
            //      //FechaCreacion = new DateTime()
            //  },
            //  new Noticia
            //  {
            //      Titulo = "Conferencia",
            //      Descripcion = "Mini",
            //      ImageUrl = "",
            //      CategoriaId = 1,
            //      Contenido = "El mi�rcoles 14 de octubre se present� Di�logo Latinoamericano / Apertura Argentina, un libro que re�ne los aportes de m�s de veinte reconocidos intelectuales, acad�micos y artistas provenientes de todas las regiones de la.",
            //      NoticiaId = 2,
            //      //FechaUltimaModificacion = new DateTime(),
            //      //FechaCreacion = new DateTime()
            //  },
            //  new Noticia
            //  {
            //      Titulo = "Economista de Fundaci�n DAR debati� en la UIA con referentes de fuerzas opositoras",
            //      Descripcion = "Economist",
            //      ImageUrl = "",
            //      CategoriaId = 1,
            //      Contenido = "Adem�s del economista Woyecheszen, en el panel moderado por Juan Manuel Mart�nez, participaron el legislador del PRO por la Ciudad de Buenos Aires y director Acad�mico de la Fundaci�n PENSAR, Iv�n Petrella; el candidato a diputado nacional por el Frente Renovador, Marco Lavagna y la candidata a diputada nacional por Progresistas, Danya Tavela.",
            //      NoticiaId = 3,
            //      //FechaUltimaModificacion = new DateTime(),
            //      //FechaCreacion = new DateTime()
            //  }
            //);
            //
            context.Clients.AddOrUpdate(new Client()
            {
                Id = "desarrWebApp",
                Secret = Helper.GetHash("abc@123"),
                Name = "DAR Front-end Application",
                ApplicationType = ApplicationTypes.JavaScript,
                Active = true,
                RefreshTokenLifeTime = 7200,
                AllowedOrigin = "http://localhost:55940"
            });

        }
    }
}
