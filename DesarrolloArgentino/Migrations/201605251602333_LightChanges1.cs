namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LightChanges1 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Noticias", "FechaCreacion", c => c.DateTime());
            AlterColumn("dbo.Noticias", "FechaUltimaModificacion", c => c.DateTime());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Noticias", "FechaUltimaModificacion", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Noticias", "FechaCreacion", c => c.DateTime(nullable: false));
        }
    }
}
