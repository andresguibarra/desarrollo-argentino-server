// <auto-generated />
namespace DesarrolloArgentino.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class SeAgregoDatosDespidos : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SeAgregoDatosDespidos));
        
        string IMigrationMetadata.Id
        {
            get { return "201606042136008_SeAgregoDatosDespidos"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
