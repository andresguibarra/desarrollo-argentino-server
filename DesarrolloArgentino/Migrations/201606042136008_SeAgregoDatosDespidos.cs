namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SeAgregoDatosDespidos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CategoriaDespidoes",
                c => new
                    {
                        CategoriaDespidoId = c.Long(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.CategoriaDespidoId);
            
            CreateTable(
                "dbo.DatosEmpresas",
                c => new
                    {
                        DatosEmpresaId = c.Long(nullable: false, identity: true),
                        NombreDeEmpresa = c.String(),
                        CantidadDeEmpleados = c.Long(nullable: false),
                        Rubro = c.String(),
                        Provincia = c.String(),
                        Ciudad = c.String(),
                        Dirección = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.DatosEmpresaId);
            
            CreateTable(
                "dbo.DatosPersonales",
                c => new
                    {
                        DatosPersonalesId = c.Long(nullable: false, identity: true),
                        NombreApellido = c.String(),
                        DireccionEMail = c.String(),
                        Teléfono = c.String(),
                        Edad = c.Int(nullable: false),
                        EstudiosProfesión = c.String(),
                        Provincia = c.String(),
                        Ciudad = c.String(),
                        Location = c.String(),
                    })
                .PrimaryKey(t => t.DatosPersonalesId);
            
            CreateTable(
                "dbo.InfoDespidoes",
                c => new
                    {
                        InfoDespidoId = c.Long(nullable: false, identity: true),
                        CategoriaId = c.Long(nullable: false),
                        DatosEmpresaId = c.Long(nullable: false),
                        DatosPersonalesId = c.Long(nullable: false),
                        OpcionesId = c.Long(nullable: false),
                        Comentarios = c.String(),
                        FuncionEmpresa = c.String(),
                    })
                .PrimaryKey(t => t.InfoDespidoId);
            
            CreateTable(
                "dbo.Opciones",
                c => new
                    {
                        OpcionesId = c.Long(nullable: false, identity: true),
                        Nombre = c.String(),
                    })
                .PrimaryKey(t => t.OpcionesId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Opciones");
            DropTable("dbo.InfoDespidoes");
            DropTable("dbo.DatosPersonales");
            DropTable("dbo.DatosEmpresas");
            DropTable("dbo.CategoriaDespidoes");
        }
    }
}
