namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedAutor : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Noticias", "Autor", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Noticias", "Autor");
        }
    }
}
