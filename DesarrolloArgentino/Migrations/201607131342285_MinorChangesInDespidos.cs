namespace DesarrolloArgentino.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MinorChangesInDespidos : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.InfoDespidoes", "OpcionesId", "dbo.Opciones");
            DropIndex("dbo.InfoDespidoes", new[] { "OpcionesId" });
            AddColumn("dbo.DatosEmpresas", "Latitude", c => c.Long(nullable: false));
            AddColumn("dbo.DatosEmpresas", "Longitude", c => c.Long(nullable: false));
            AddColumn("dbo.DatosPersonales", "Telefono", c => c.String());
            AddColumn("dbo.DatosPersonales", "Ocupacion", c => c.String());
            AddColumn("dbo.InfoDespidoes", "OpcionDespido", c => c.Int(nullable: false));
            AlterColumn("dbo.DatosEmpresas", "CantidadDeEmpleados", c => c.Long());
            AlterColumn("dbo.DatosPersonales", "Edad", c => c.Long());
            DropColumn("dbo.DatosEmpresas", "Location");
            DropColumn("dbo.DatosPersonales", "Teléfono");
            DropColumn("dbo.DatosPersonales", "EstudiosProfesión");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DatosPersonales", "EstudiosProfesión", c => c.String());
            AddColumn("dbo.DatosPersonales", "Teléfono", c => c.String());
            AddColumn("dbo.DatosEmpresas", "Location", c => c.String());
            AlterColumn("dbo.DatosPersonales", "Edad", c => c.Int(nullable: false));
            AlterColumn("dbo.DatosEmpresas", "CantidadDeEmpleados", c => c.Long(nullable: false));
            DropColumn("dbo.InfoDespidoes", "OpcionDespido");
            DropColumn("dbo.DatosPersonales", "Ocupacion");
            DropColumn("dbo.DatosPersonales", "Telefono");
            DropColumn("dbo.DatosEmpresas", "Longitude");
            DropColumn("dbo.DatosEmpresas", "Latitude");
            CreateIndex("dbo.InfoDespidoes", "OpcionesId");
            AddForeignKey("dbo.InfoDespidoes", "OpcionesId", "dbo.Opciones", "OpcionesId", cascadeDelete: true);
        }
    }
}
