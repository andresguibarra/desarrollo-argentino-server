﻿using System;
using System.Configuration;
using System.Net.Mail;
using System.Threading.Tasks;

namespace DesarrolloArgentino.Services
{
    public class MailService


    {

        public static async Task SendMail(string nombre, string message, string asunto, string mail)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(mail),
                Body = "De: " + nombre
                       + Environment.NewLine
                       + "Mail: " + mail
                       + Environment.NewLine
                       + message
            };
            mailMessage.To.Add(ConfigurationManager.AppSettings["mail:contacto"]);
            mailMessage.Subject = "Mensaje de Desarrollo Argentino: " + asunto;

            SmtpClient smtp = new SmtpClient();
            await smtp.SendMailAsync(mailMessage);
        }
        public static async Task SendMail(string nombre, string message, string asunto, string mail, string paraEmail)
        {
            var mailMessage = new MailMessage
            {
                From = new MailAddress(mail),
                Body = "De: " + nombre
                       + Environment.NewLine
                       + "Mail: " + mail
                       + Environment.NewLine
                       + message
            };
            mailMessage.To.Add(paraEmail);
            mailMessage.Subject = "Mensaje de Desarrollo Argentino: " + asunto;

            SmtpClient smtp = new SmtpClient();
            await smtp.SendMailAsync(mailMessage);
        }
    }
}