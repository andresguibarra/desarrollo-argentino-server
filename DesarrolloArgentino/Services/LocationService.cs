﻿using DesarrolloArgentino.Models;
using GoogleMapsApi;
using GoogleMapsApi.Entities.Geocoding.Request;
using GoogleMapsApi.Entities.Geocoding.Response;

namespace DesarrolloArgentino.Services
{
    public static class LocationService
    {
        private const string UrlApiGoogle = "https://maps.googleapis.com";


        public static LocationModel GetLocation(string address, string province, string city)
        {

            //using (var client = new HttpClient())
            //{

            //    LocationGoogleObject model = null;
            //    client.BaseAddress = new Uri(UrlApiGoogle);

            //    var values = new List<KeyValuePair<string, string>>();
            //    //values.Add(new KeyValuePair<string, string>("AccCode", rb.AccCode));
            //    values.Add(new KeyValuePair<string, string>("address", address + ", " + city + ", " + province));
            //    values.Add(new KeyValuePair<string, string>("components", "country:AR"));
            //    var content = new FormUrlEncodedContent(values);
            //    var querystring = content.ReadAsStringAsync().Result;
            //    var task = client.PostAsync("/maps/api/geocode/json", content).ContinueWith((taskwithresponse) =>
            //      {
            //          var response = taskwithresponse.Result;
            //          var jsonString = response.Content.ReadAsStringAsync();
            //          jsonString.Wait();
            //          model = JsonConvert.DeserializeObject<LocationGoogleObject>(jsonString.Result);

            //      });
            //    task.Wait();
            //    return model;
            //}

            GeocodingRequest geocodeRequest = new GeocodingRequest()
            {
                Address = address + ", " + city + ", " + province,
                Region = "AR"
            };
            var geocodingEngine = GoogleMaps.Geocode;
            GeocodingResponse geocode = geocodingEngine.Query(geocodeRequest);


            return new LocationModel() { Latitude = ((GoogleMapsApi.Entities.Geocoding.Response.Result[])geocode.Results)[0].Geometry.Location.Latitude, Longitude = ((GoogleMapsApi.Entities.Geocoding.Response.Result[])geocode.Results)[0].Geometry.Location.Longitude };

        }


    }
}